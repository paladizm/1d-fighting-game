#include "Arduino.h"
#include "Player.h"
#include "HumanController.h"

HumanController::HumanController(
    Player* playerToControl,
    byte left, byte right,
    byte stanceUp, byte stanceDown, byte attack) {

    player = playerToControl;

    moveLeftPin = left;
    moveRightPin = right;
    stanceUpPin = stanceUp;
    stanceDownPin = stanceDown;
    attackPin = attack;
    
    pinMode(moveLeftPin, INPUT);
    pinMode(moveRightPin, INPUT);
}

void HumanController::update() {
    int moveLeftPinValue = digitalRead(moveLeftPin);
    int moveRightPinValue = digitalRead(moveRightPin);

    player->velocity = 0;

    if (moveLeftPinValue == LOW) {
        player->velocity = -1;
    }

    if (moveRightPinValue == LOW) {
        player->velocity = 1;
    }
}

void HumanController::print() {
    Serial.print("Controller got player ");
    Serial.println(player->number);
}
