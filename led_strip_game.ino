#include "Adafruit_NeoPixel.h"
#include "Player.h"
#include "HumanController.h"

#define LED_DATA_PIN 7
#define NUMBER_OF_LEDS 23

Adafruit_NeoPixel strip = Adafruit_NeoPixel(
    NUMBER_OF_LEDS,
    LED_DATA_PIN,
    NEO_GRB + NEO_KHZ800);

Player player1 = Player(222, 0, 0);
HumanController player1Controller(&player1, 2, 4, 0, 0, 0);

unsigned long currentTime = 0;
unsigned long lastTime = 0;
unsigned long deltaTime = 0;

void setup() {
    Serial.begin(9600);
    Serial.println("1D fighter");

    printPlayerInfo(player1);
    player1Controller.print();

    strip.begin();
    strip.show();

    currentTime = millis();
    lastTime = currentTime;
}

void printPlayerInfo(Player player) {
    Serial.println("------------");
    Serial.print("Player: ");
    Serial.print(player.number);
    Serial.print(" position ");
    Serial.print(player.position);

    Serial.println("");
}

void loop() {
    currentTime = millis();
    deltaTime = currentTime - lastTime;
    lastTime = currentTime;

    player1Controller.update();

    player1.update(deltaTime);

    render();
}

void render() {
    clearStrip(strip.Color(0, 0, 0));

    strip.setPixelColor(player1.position / 1000, strip.Color(255, 0, 0));

    strip.show();
}

void clearStrip(uint32_t c) {
  for(uint16_t i=0; i<strip.numPixels(); i++) {
      strip.setPixelColor(i, c);
  }
}

void colorWipe(uint32_t c, uint8_t wait) {
  for(uint16_t i=0; i<strip.numPixels(); i++) {
      strip.setPixelColor(i, c);
      strip.show();
      delay(wait);
  }
}
