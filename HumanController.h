class HumanController {
    public:
    HumanController(Player*, byte, byte, byte, byte, byte);
    void update();
    void print();

    Player* player;

    byte moveLeftPin;
    byte moveRightPin;

    byte stanceUpPin;
    byte stanceDownPin;
    byte attackPin;
};
