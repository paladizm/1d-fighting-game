#include "Arduino.h"
#include "Player.h"

Player::Player(int playerNumber, int playerColor, int startingPosition) {
    number = playerNumber;
    color = playerColor;
    position = startingPosition;

    velocity = 0;
}

void Player::update(unsigned long deltaTime) {
    position += deltaTime * velocity;
}
